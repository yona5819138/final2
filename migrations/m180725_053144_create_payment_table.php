<?php

use yii\db\Migration;

/**
 * Handles the creation of table `payment`.
 */
class m180725_053144_create_payment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('payment', [
            'id' => $this->primaryKey(),
            'date' => $this->timestamp(),
            'resident' => $this->string(),
            'paid' => $this->boolean(),
            'sum' => $this->integer(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('payment');
    }
}
