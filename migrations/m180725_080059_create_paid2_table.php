<?php

use yii\db\Migration;

/**
 * Handles the creation of table `paid2`.
 */
class m180725_080059_create_paid2_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('paid2', [
            'id' => $this->primaryKey(),
            'name'=> $this->string(),
             'idd'=> $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('paid2');
    }
}
