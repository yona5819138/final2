<?php

use yii\db\Migration;

/**
 * Handles the creation of table `paid`.
 */
class m180725_074834_create_paid_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('paid', [
            'id' => $this->primaryKey(),
            'name'=> $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('paid');
    }
}
