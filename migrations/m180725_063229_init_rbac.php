<?php

use yii\db\Migration;

/**
 * Class m180725_063229_init_rbac
 */
class m180725_063229_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
     public function safeUp()
    { $auth = Yii::$app->authManager;//חובה תמיד
        
      
        $resident = $auth->createRole('resident');
        $auth->add($resident);

        $vaad  = $auth->createRole('vaad ');
        $auth->add($vaad );
   

        $auth->addChild($vaad, $resident);

   ////////////////////////////////////////////////////////////////////////////
        
   $createPayment = $auth->createPermission('createPayment');//vaad
   $auth->add($createPayment);

   $deletePayment = $auth->createPermission('deletePayment');//vaad
   $auth->add($deletePayment);
    

   $manageUsers = $auth->createPermission('manageUsers'); //createResident deleteResident updateResident
   $auth->add($manageUsers);

   $viewResident = $auth->createPermission('viewResident');
   $auth->add($viewResident);

   $indexResident = $auth->createPermission('indexResident');
   $auth->add($indexResident);  

   $viewPayment = $auth->createPermission('viewPayment');
   $auth->add($viewPayment);

   $indexPayment = $auth->createPermission('indexPayment');
   $auth->add($indexPayment);  

  
   $updatePayment = $auth->createPermission('updatePayment');
   $auth->add($updatePayment);

   $updateOwnPayment = $auth->createPermission('updateOwnPayment');

   $rule = new \app\rbac\ResidentRule;// לקרוא לזה איך שקראנו לו בתוך תיקיית rbac
   $auth->add($rule);
   
   $updateOwnPayment->ruleName = $rule->name;  //מה שאנחנו עושים עליו את התנאי יופיע ראשון               
   $auth->add($updateOwnPayment);                                                    
   
   $auth->addChild($vaad, $manageUsers);
   $auth->addChild($vaad, $createPayment);
   $auth->addChild($vaad, $deletePayment);
   $auth->addChild($resident, $viewResident);
   $auth->addChild($resident, $indexResident);
   $auth->addChild($resident, $indexPayment); 
   $auth->addChild($resident, $viewPayment);
  
   $auth->addChild($resident, $updateOwnPayment);
   $auth->addChild($updateOwnPayment, $updatePayment);   //קודם כל המצומצם ואז הגדול
   
   
   
   
   
   
   
   
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180725_063229_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180725_063229_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
