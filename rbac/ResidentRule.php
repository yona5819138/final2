<?php
namespace app\rbac;

use yii\rbac\Rule;
use app\models\User;

/**
 * Checks if memberID matches user passed via params
 */
class ResidentRule extends Rule
{
    public $name = 'isResident';// לשנות בהתאם לדבר שעליו יש תנאי

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        return isset($params['user']) ? $params['user']->id == $user : false;// לשים בתוך הפארמס את מה שאנחנו בעצם מגבילים בתנאי
    }
}

?>
