<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\db\ActiveRecord;
use app\models\User;
use app\models\Payment;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Payment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date')->textInput() ?>

   


   
     <?= $form->field($model, 'resident')->dropDownList(
        ArrayHelper::map(User::find()->asArray()->all(), 'id', 'name')    ) ?>


    <?= $form->field($model, 'paid')->dropDownList($model->user2); ?>

    <?= $form->field($model, 'sum')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
